#!/usr/bin/env python3

import os
import config as cfg
from db.db_apis import DB_Api
from utils.helper import Helper
from utils import progressbar
from utils.helper import doTryCatch
import concurrent.futures

extensions_list = lambda x: x in cfg.extensions_list

class Catalog(Helper):
    symbols = "└──"

    def __init__(self):
        super().__init__()
        self.db = DB_Api()
        self.conn = self.db.connect()
        self.files_added = 0
        self.files_processed = 0
        self.crashes = []
        self.tag_supported_images = lambda x: x[1::] in cfg.tag_supported_images
        self.supported_images = lambda x: x[1::] in cfg.supported_images
        self.cardInfo_images = lambda x: x[1::] in cfg.cardinfo_supported_images

    def startTransversion(self, username):
        """
        Start from each role in config.py
        :return:
        """
        self._save_albums(username)

        #iterate the folders in config.py folders list
        for root in cfg.included_folders:
            self.transverse_root(root)

    def transverse_root(self, initial_folder):
        """
        Walk down into folder path informed.
        Adds the files' reference to DB
        If the same file is already loaded (even with different name or location),
        insert to DB and mark both as duplicated
        :param folder path:
        :return :
        """
        assert os.path.isdir(initial_folder)

        # start 5 different treads
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
            for dir_path, dirs, files in os.walk(top=initial_folder, topdown=True):
                dirs[:] = [d for d in dirs if not d[0] == '.']      #skip hidden folders
                if not len(files):
                    continue
                future_theread = executor.submit(self.process_folder, dirs, dir_path)
                print(future_theread.result())

    #@doTryCatch
    def process_folder(self, dirs, dir_path):
        self.file_record['dir_path'] = dir_path
        print(self.file_record['dir_path'])

        # drill down each folder
        for folder in progressbar.progress_bat(dirs, "processing folders"):
            list_of_files = os.listdir(os.path.join(dir_path,folder))

            for filename in progressbar.progress_bat(list_of_files, f"{self.symbols} processing files"):
                if not filename.rfind(".",0,1): continue #skip hidden files
                if not extensions_list(os.path.splitext(filename)[1]):
                    continue
                self.process_file(filename, os.path.join(dir_path,folder))

    #@doTryCatch
    def process_file(self, filename, dir_path):
        """
        process file
        Filter supported file types and images
        :return:
        """
        # to print file name indented
        spaces = len(self.file_record['dir_path'])
        print_folder = True  #print folder one  TODO add log system

        #  clean up before new folder
        self.files_processed += 1
        self.file_record['image'] = "ObjectId"
        self.file_record['metadata_id'] = "ObjectId"
        self.file_record['file_info'] = "ObjectId"
        self.file_record['card_info'] = "ObjectId"
        self.file_record['thumbnail_id'] = "ObjectId"

        self.file_record['filename'] = filename
        self.file_record['file_path'] = os.path.join(dir_path, filename)

        if print_folder:
            print(f"\n{' ' * spaces}└── {filename}")
        print_folder = False

        self.data = {} #holds payload for saving

        # file info, creationg, size, etc
        if file_info := self._getFileInfo():
            self.data["file_info"] = file_info
            assert f"Could not get the file info from {self.file_record['file_path']}"

        # save file to get the id
        if file_id := self._save_file_to_db():
            self.data["file_id"] = file_id
            assert f"Could not save the file {self.file_record['file_path']}"

        # save image if in expected file extension
        if record_id := self.supported_images(os.path.splitext(filename)[1]):
            self.data["thumbnail_id"] = self._savePhotoThumbnail()
            self.file_record["thumbnail_id"] = self.data["thumbnail_id"]
            assert f"Could not get the file thumbnail_id from {self.file_record['file_path']}"

        # get metadata info from photos
        if metadata_id := self.prepare_to_save_metadata(self.file_record['filename']):
            self.data["metadata_id"] = metadata_id
            self.file_record["metadata_id"] = self.data["metadata_id"]
            assert f"Could not get the file metadata_id from {self.file_record['file_path']}"

        # update file document with current changes
        if update := self._replace_file():
            assert f"Could not update the file from {self.data}"

        # if the file exists in db, add 1 to duplicate
        self._update_duplicate_tag()

