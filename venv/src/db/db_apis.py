#!/usr/bin/env python
#  Renato Maschion
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

# from wand.display import display
import binascii
import os
import traceback
# import cStringIO
from io import BytesIO

import gridfs
from PIL import Image

from .start_db import Start_DB


class DB_Api(Start_DB):

    def __init__(self):
        super().__init__()

    def doTryCatch(function):
        def decorated_function(*args):
            try:
                return function(*args)
            except Exception as e:
                args[0].crashes.append(e)
                track = traceback.format_exc()
                args[0].crashes.append(track)

        return decorated_function

    @doTryCatch
    def insert_if_not_found(self, collection, key, data):
        """
        If file doesn't exist, add the document to collection
        :param collection:
        :param key:
        :param data object:
        :return document id:
        """
        try:
            post = self.db[collection]
            res = post.update(key, {**key, **data}, upsert=True)
            if res['updatedExisting']:
                id = self.find_one(collection, key)['_id']
            else:
                id = res['upserted']
        except Exception as e:
            print(e)
            return None
        return id

    @doTryCatch
    def get_file_hash(self, file_hash):
        """
        Retrieve hash_id given a file hash
        :param file hash:
        :return hash_id:
        """
        id = self.fetch_id('hashes', {'hash': file_hash})
        if id:
            return id
        else:
            return None

    def fetch_id(self, collection, criteria):
        """
        Find and return document's ID
        :param collection name:
        :param criteria query:
        :return: document id:
        """
        res = self.db[collection].find_one(criteria)
        if not res:
            # print(f'FETCH not found {criteria.items()}')
            return None
        return res['_id']

    # @doTryCatch
    def find_one(self, collection, data):
        """
        Retrieve ONE record giving the data object informed
        :param collection name:
        :param data query:
        :return: file document:
        """
        post = self.db[collection]
        result = post.find_one(data)
        if result:
            return result
        else:
            return None

    def insert_one_api(self, collection, data, bypass_document_validation=False):
        """
        Add ONE document to a collection
        :param collection name:
        :param data object:
        :return document info:
        """
        try:
            post = self.db[collection]
            # data = dict(map(lambda i: i if isinstance(i, object) else (str(i[0]), str(i[1])), data.items()))
            if not data:
                print('stop)')
            res = post.insert_one(data)  # , bypass_document_validation)
            if res:
                return res.inserted_id
            return None
        except Exception as e:
            assert e

    @doTryCatch
    def insertMany(self, collection, data):
        """
        inserts one or more document at the same time
        :param collection:
        :param data object:
        :return document _id:
        """
        post = self.db[collection]
        res = post.insert_many(data)
        _id = None
        if len(res.inserted_ids):
            _id = res.inserted_ids[0]
        if res:
            return _id
        return None

    @doTryCatch
    def update_one(self, collection, myquery, data):
        """
        Update existent record
        :param the_hash:
        :return hash_id:
        """
        post = self.db[collection]
        res = post.update_one(myquery, data, upsert=True)
        if res.raw_result['updatedExisting']:
            return myquery['_id']
        else:
            return res.upserted_id

    @doTryCatch
    def replace_one(self, collection, myquery, data):
        """
        Update existent record
        :param the_hash:
        :return hash_id:
        """
        post = self.db[collection]
        res = post.replace_one(myquery, data, upsert=True)
        if res:
            return res
        return None

    @doTryCatch
    def count_documents(self, collection, data):
        """
        count number of documents given a data query
        :param collection:
        :param data object:
        :return number of documents fround:
        """
        count = self.db[collection].count_documents(data)
        if not count:
            return None
        return count

    @doTryCatch
    def find_all(self, collection, criteria):
        """
        Find all records matching the given criteria
        :param collection:
        :param criteria query:
        :return documents found:
        """
        res = self.db[collection].find(criteria)
        if not res:
            return None
        return res

    #@doTryCatch
    def saveImage(self, file_path, buf):
        grid_fs = gridfs.GridFS(self.db)
        with open(file_path, 'rb') as f:
            contents = f.read()
        id = grid_fs.put(contents, filename="file")
        # id = grid_fs.put(name=os.path.basename(file_path), data=buf)
        return id

    def saveFiles(self, file: str, file_info: {}) -> int:
        if id := self.insert_one_api(file, file_info):
            assert id
        return id

    def isFileRecord(self, recordFields):
        if record := self.find_one('files', recordFields):
            return record['_id']
        return None
