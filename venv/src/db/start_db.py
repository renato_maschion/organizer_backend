#!/usr/bin/env python
#  Renato Maschion
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import sys

from pymongo import MongoClient
#from pymongo_document import conf
#from pymongo import errors

# import sqlite3
# from sqlite3 import Error
from .logger import Logger

from src import config


class Start_DB():
    class __Singleton:
        """
        Create on instance of the class for every run
        """
        def __init__(self):
            self.verbose_stream = sys.stdout  # if self._options.be_verbose else None
            self._verbose = Logger('Database Starting', self.verbose_stream)
        def __str__(self):
            return repr(self)

    instance    = None
    def __init__(self):
        if not Start_DB.instance:
            Start_DB.instance   = Start_DB.__Singleton()

            Start_DB.client = self.connect()
            Start_DB.db     = self.create_a_database()

            #self._verbose('- Database is connected')

    def connect(self) -> object:
        return MongoClient(*config.mongodb_path)

    def create_a_database(self) -> object:
        return self.client[config.db_name]

    def get_collection(self, name) -> object:
        return self.db[name]

    def __getattr__(self, name):
        return getattr(self.instance, name)
