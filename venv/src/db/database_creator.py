#!/usr/bin/env python
#  Renato Maschion
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from sqlite3 import OperationalError

########################
#   CREATE TABLES
########################

class DatabaseCreator():

    def __init__(self, db):
        self.db = db
        self._init_schema()

    def _init_schema(self):
        self._create_table_files()
        self._create_table_category()
        self._create_table_hashes()
        self._create_table_photoTags()
        self._create_table_cardInfo()
        self._create_table_keywords()
        self.db.conn.commit()

    def _create_table_files(self):
        self._create_table('files', {
            'hash': 'TEXT NOT NULL',
            'name': 'TEXT',
            'path': 'TEXT',
            'dupl' : 'INTEGER',
        }, ('hash','name','path'))

    def _create_table_photoTags(self):
        self._create_table('photoTags', {
            'photo_id'          : 'INTEGER',
            'ExifVersion'       : 'INTEGER',
            'ShutterSpeedValue' : 'TEXT',
            'ApertureValue'     : 'TEXT',
            'DateTimeOriginal'  : 'TEXT',
            'DateTimeDigitized' : 'TEXT',
            'ExposureBiasValue' : 'TEXT',
            'MaxApertureValue'  : 'TEXT',
            'MeteringMode'      : 'INTEGER',
            'ColorSpace'        : 'INTEGER',
            'Flash'             : 'INTEGER',
            'FocalLength'       : 'INTEGER',
            'ExifImageWidth'    : 'INTEGER',
            'ExifImageHeight'   : 'INTEGER',
            'SceneCaptureType'  : 'INTEGER',
            'FocalPlaneXResolution' : 'TEXT',
            'FocalPlaneYResolution' : 'TEXT',
            'FocalPlaneResolutionUnit' : 'INTEGER',
            'SubsecTimeOriginal'    : 'TEXT',
            'SubsecTimeDigitized'   : 'TEXT',
            'ImageLength'       : 'INTEGER',
            'BitsPerSample'     : 'TEXT',
            'Make'              : 'TEXT',
            'Model'             : 'TEXT',
            'Orientation'       : 'INTEGER',
            'SamplesPerPixel'   : 'INTEGER',
            'ExposureTime'      : 'TEXT',
            'XResolution'       : 'TEXT',
            'YResolution'       : 'TEXT',
            'FNumber'           : 'TEXT',
            'ExposureProgram'   : 'INTEGER',
            'CustomRendered'    : 'INTEGER',
            'ISOSpeedRatings'   : 'INTEGER',
            'ResolutionUnit'    : 'INTEGER',
            'PhotometricInterpretation' : 'INTEGER',
            'ExposureMode'      : 'INTEGER',
            'ImageWidth'        : 'INTEGER',
            'WhiteBalance'      : 'INTEGER',
            'BodySerialNumber'  : 'TEXT',
            'LensSpecification' : 'TEXT',
            'Software'          : 'TEXT',
            'LensModel'         : 'TEXT',
            'DateTime'          : 'TEXT',
            'ExifOffset'        : 'INTEGER',
        },())


    def _create_table_hashes(self):
        self._create_table('hashes', {
            'hash': 'TEXT NOT NULL',
        }, ('hash',))

    def _create_table_category(self):
        self._create_table('category', {
            'name': 'TEXT NOT NULL',
        }, ('name',))

    def _create_table_keywords(self):
        self._create_table('keywords', {

        },
    def _create_table_cardInfo(self):
        self._create_table('cardInfo', {
            'title'              : 'TEXT',
            'subtitle'           : 'TEXT',
            'primary'            : 'TEXT',
            'location'           : 'TEXT',
            'text'               : 'TEXT',
        }

    def _create_table(self, table_name, columns, unique_columns=()):
        definitions = ['id INTEGER PRIMARY KEY']
        for column_name, properties in columns.items():
            definitions.append('%s %s' % (column_name, properties))
        if unique_columns:
            unique_column_names = ', '.join(unique_columns)
            definitions.append('CONSTRAINT unique_%s UNIQUE (%s)' % (
                table_name, unique_column_names)
            )
        try :
            sql_statement = 'CREATE TABLE %s (%s)' % (table_name, ', '.join(definitions))
            self.db.conn.execute(sql_statement)
        except OperationalError as e:
            if "already exists" in e.args[0]:
                self._add_new_columns(table_name, columns)
            else:
                raise e

    def _add_new_columns(self, table, columns):
        cursor = self.db.conn.execute(f'select * from {table}')
        names = list(map(lambda x: x[0], cursor.description))
        new_fields = []
        new_fields = [f for f,_ in columns.items() if not f in names and not f == 'id']
        for f in new_fields:
            sql_statement = f'ALTER TABLE {table} ADD COLUMN {f} {columns[f]}'
            self.db.conn.execute(sql_statement)

    def rename_table(self, old_name, new_name):
        sql_statement = 'ALTER TABLE %s RENAME TO %s' % (old_name, new_name)
        self.db.conn.execute(sql_statement)

    def drop_table(self, table_name):
        sql_statement = 'DROP TABLE %s' % table_name
        self.db.conn.execute(sql_statement)

    def copy_table(self, from_table, to_table, columns_to_copy):
        column_names = ', '.join(columns_to_copy)
        sql_statement = 'INSERT INTO %s(%s) SELECT %s FROM %s' % (
            to_table,
            column_names,
            column_names,
            from_table
        )
        self.db.conn.execute(sql_statement)

    def commit(self):
        self.db.conn.commit()