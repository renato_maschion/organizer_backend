#!/usr/bin/env python
#  Renato Maschion
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software

#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from __future__ import print_function
import os
import sys
from loader import Catalog
from optparse import OptionParser
import config as cfg

sys.path.append(os.path.abspath(__file__ + '/../..'))
import config as config

from db.db_apis import DB_Api

from db.logger import Logger

class Run(object):
    def __init__(self):
        self.config()
        self.verbose_stream = sys.stdout #if self._options.be_verbose else None
        #connect to db apis. it will also start DB
        self.db = DB_Api()

    def config(self):
        """
        Sets arguments to configuration variables
        if arguments not informed it will consider the ones in config.py
        :return:
        """
        parser = OptionParser()
        parser.add_option("-r", "--roots", dest="root",
                          help="list of roots to process", metavar="FILE")
        parser.add_option("-f", "--folders", dest="folders",
                          help="list of folders to process", metavar="FILE")
        parser.add_option("-k", "--kw", dest="kw",
                          help="Photo categories", metavar="FILE")
        parser.add_option("-e", "--ext", dest="ext",
                          help="list of file extension to process", metavar="FILE")
        parser.add_option("-q", "--quiet",
                          action="store_false", dest="verbose", default=True,
                          help="don't print status messages to stdout")

        (options, args) = parser.parse_args()

        # cfg.included_roots = set((options.roots.split(','))) if options.root else cfg.included_roots
        cfg.included_folders = set((options.folders.split(','))) if options.folders else cfg.included_folders
        cfg.extensions_list = set((options.ext.split(','))) if options.ext else cfg.extensions_list

    def run(self):
        """
        Walk through all folders and files passed as arguments
        run() <location>foldername <location>foldername ...
        :return:
        """
        catalog = Catalog()
        catalog.startTransversion("renato")
        print(f"{'*' * 30} - Done Running the app! - {'*' * 30}")
        print(f"Files processed: {catalog.files_processed}")
        print(f"Files added: {catalog.files_added}")
        print(f"{'*' * 30} - ERRORS! - {'*' * 30}")
        for i in catalog.crashes:
            print(i)
        print(f"{'*' * 30} - All Done! - {'*' * 30}")
if __name__ == '__main__':
    Run().run()