###
# FILES schema
###
file_schema = {
    '_id': 'ObjectId',
    'image': 'ObjectId',
    'album_id': 'ObjectId',
    'file_path': 'String',
    'dir_path': 'String',
    'hash_id': 'ObjectId',
    'file_info': 'document',
    'card_info': 'document',
    'thumbnail_id': 'ObjectId',
    'metadata_id': 'ObjectId'
}

###
# ALBUMS schema
###
albums_schema = {
    '_id': 'ObjectId',
    'name': 'String',
    'username': 'String'
}

###
# HASHES SCHEMA
###
hashes_schema = {
    "_id": 'ObjectId',
    'hash': 'string'
}

###
# IMAGE.FILES schema
###
image_files_schema = {
    "_id": 'ObjectId',
    "width": 'int32',
    "height": 'int32',
    "format": "String",
    "thumbnail_id": 'ObjectId',
    "filename": 'String',
    "path": 'String',
    "md5": 'String',
    "chunkSize": 'int32',
    "length": 'int32',
    'keywords': 'document',
    "uploadDate": 'Date'
}

###
# IMAGE.CHUNKS schema
###
image_chunks_schema = {
    "_id": 'ObjectId',
    "files_id": 'ObjectId',
    'n': 'int32',
    "data": "binary"
}

###
# USERS schema
###
users_schema = {
    '_id': 'ObjectId',
    'username': 'ObjectId',
    'username': 'string',
    'password': 'string',
    'first': 'string',
    'last': 'string',
    'createdAt': 'string',
    'updatedAt': 'string'
}

###
# KEYWORDS schema
###
keywords_schema = {
    '_id': 'ObjectId',
    'image_id': 'ObjectId',
    'keywords': ['string']
}

###
# METADATA_TAG schema
###
metadata_tag_schema = {
    "_id": 'ObjectId',
    'ExitVersion': "b",
    'photo_id': 'ObjectId',
    'ExifVersion': 'int32',
    'ShutterSpeedValue': 'String',
    'ApertureValue': 'String',
    'DateTimeOriginal': 'String',
    'DateTimeDigitized': 'String',
    'ExposureBiasValue': 'String',
    'MaxApertureValue': 'String',
    'MeteringMode': 'int32',
    'ColorSpace': 'int32',
    'Flash': 'int32',
    'FocalLength': 'int32',
    'ExifImageWidth': 'int32',
    'ExifImageHeight': 'int32',
    'SceneCaptureType': 'int32',
    'FocalPlaneXResolution': 'String',
    'FocalPlaneYResolution': 'String',
    'FocalPlaneResolutionUnit': 'int32',
    'SubsecTimeOriginal': 'String',
    'SubsecTimeDigitized': 'String',
    'ImageLength': 'int32',
    'BitsPerSample': 'String',
    'Make': 'String',
    'Model': 'String',
    'Orientation': 'int32',
    'SamplesPerPixel': 'int32',
    'ExposureTime': 'String',
    'XResolution': 'String',
    'YResolution': 'String',
    'FNumber': 'String',
    'ExposureProgram': 'int32',
    'CustomRendered': 'int32',
    'ISOSpeedRatings': 'int32',
    'ResolutionUnit': 'int32',
    'PhotometricInterpretation': 'int32',
    'ExposureMode': 'int32',
    'ImageWidth': 'int32',
    'WhiteBalance': 'int32',
    'BodySerialNumber': 'String',
    'LensSpecification': 'String',
    'Software': 'String',
    'LensModel': 'String',
    'DateTime': 'String',
    'ExifOffset': 'int32',
}
