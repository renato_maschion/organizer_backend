##################
# tag supported images
##################
tag_supported_images = {
    'rgb': 'SGI ImgLib Files',
    #'gif': 'GIF 87a and 89a Files',  NOT SUPPORTED
    'pbm': 'Portable Bitmap Files',
    'pgm': 'Portable Graymap Files',
    'ppm': 'Portable Pixmap Files',
    'tiff': 'TIFF Files',
    'rast': 'Sun Raster Files',
    'xbm': 'X Bitmap Files',
    'jpeg': 'JPEG data in JFIF or Exif formats',
    'jpg': 'JPG data',
    'bmp': 'BMP files',
    'png': 'Portable Network Graphics',
    'webp': 'WebP files',
    'exr': 'OpenEXR Files',
}
##################
# Card Photo supported files
##################
cardinfo_supported_images = {
    'rgb': 'SGI ImgLib Files',
    'gif': 'GIF 87a and 89a Files',
    'tiff': 'TIFF Files',
    'xbm': 'X Bitmap Files',
    'jpeg': 'JPEG data in JFIF or Exif formats',
    'jpg': 'JPG data',
    'bmp': 'BMP files',
    'png': 'Portable Network Graphics',
}

##################
# supported images
##################
supported_images = {
    'rgb': 'SGI ImgLib Files',
    #'gif': 'GIF 87a and 89a Files',  NOT SUPPORTED
    'pbm': 'Portable Bitmap Files',
    'pgm': 'Portable Graymap Files',
    'ppm': 'Portable Pixmap Files',
    'tiff': 'TIFF Files',
    'rast': 'Sun Raster Files',
    'xbm': 'X Bitmap Files',
    'jpeg': 'JPEG data in JFIF or Exif formats',
    'jpg': 'JPG data',
    'bmp': 'BMP files',
    'png': 'Portable Network Graphics',
    'webp': 'WebP files',
    'exr': 'OpenEXR Files',
    'png': 'Portable Network Graphics',
}


#########################
# DATABASE
#########################
mongodb_path  = 'localhost', 27017
db_name       = 'photos_celia_db'

#########################
# Collections
#########################
collections = [
    'files',
    'hashes',
    'photo-tags',
    'thumbnails'
    ]

#########################
# ROOTs TO BE PROCESSED
#########################
included_folders = (("/Users/rmaschion/Desktop/2014","/Users/rmaschion/Desktop/2015",))
#set(("/Volumes/celia_home",))   #last comma is necessary if only one element

# #########################
# # FOLDERS TO BE CRAWLED
# #########################
# included_folders = set(( '2014','2015',))    #"CELIA_PERSONAL",)) #last comma is necessary if only one element
# #included_folders = set(( "test",))

#########################
# EXTENSIONS FOR CRAWL
#########################
extensions_list = set((".jpeg",".jpg",".ai",".pdf",".zip",".cr2",".png",".raw", ".docx")) #last comma is necessary if only one element

#########################
# ALbums
#########################
album_name = "celia_personal"


#########################
# Image
# MUST BE PASSED FROM UI OPTIONS
#########################
from PIL import ImageFont

image_settings = {
                "font": ImageFont.truetype("Sacramento-Regular.ttf", size=72),
                "text": "QuipApps",
                "position": (10,10)
                }