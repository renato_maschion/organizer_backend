from tqdm import tqdm

def progress_bat(files, header):
    """
    displays a bar processing files
    :param files: file listpython -m pip freeze
    :return:
    """
    return tqdm(files, desc=header, total=len(files), dynamic_ncols=True)
