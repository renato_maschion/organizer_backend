
import sys
from PIL import ImageDraw
import config as cfg

class Watermark:
    def __init__(self):
        self.font = cfg.image_settings["font"]
        self.text_watermark = cfg.image_settings["text"]
        self.position = cfg.image_settings["position"]

    def set_watermark(self, img, kargs=None) -> object:
        """
        Draw a wathermak text into image
        :param *img: the image
        :param font: font family must be present in system
        :param text: The text to burn on image
        :param position: Text position (0,0)
        :return:
        """
        #kargs can overwrite config
        if kargs:
            self.font = kargs.get("font", self.font)
            self.text_watermark = kargs.get("text", self.text_watermark)
            self.position = kargs.get("position", self.position)

        self.image: object = img;
        try:
            idraw = ImageDraw.Draw(self.image)

            #add text to image
            idraw.text(self.position, self.text_watermark, font=self.font)

            return self.image

        except IOError:
            print("Unable to create wantermark")
            print(self)
            sys.exit(1)

    def show(self):
        """
        display image at current state
        :return: open image in browser
        """
        self.image.show()

    def __repr__(self):
        return f"variables " \
               f"font:{self.font_family} " \
               f"text:{self.text_watermark} " \
               f"position: (self.position"

    def __str__(self):
        return f"Passed Arguments:" \
               f"font:{self.font_family} " \
               f"text:{self.text_watermark} " \
               f"position: (self.position"
