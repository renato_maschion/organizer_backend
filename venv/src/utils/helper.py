#!/usr/bin/env python3

import hashlib
import os
import sys
import time
import functools
import traceback
import schemas

import imghdr
from PIL import Image
from PIL.ExifTags import TAGS
from utils.image_creator import create_thumbnail_bytesIO
from utils.image_wathermark import Watermark
import config as cfg
from db.db_apis import DB_Api
import gridfs

from collections import namedtuple

def doTryCatch(func):

    @functools.wraps(func)
    def decorated_function(*args):
        try:
            return func(*args)
        except Exception as e:
            try:
                args[0].crashes.append(e)
                track = traceback.format_exc()
                args[0].crashes.append(track)
                args[0].crashes.append(*args)
            except:
                pass

    return decorated_function


class Helper:

    def __init__(self):
        ###
        # FILES schema
        ###
        INITIAL_SIZE = 8
        # two ways to create a dict
        self.file_record = {}.fromkeys(schemas.file_schema, '' * INITIAL_SIZE)
        # file_record = dict(zip(file_schema, [''] * INITIAL_SIZE))
        self.file_record['count'] = 0
        self.db = DB_Api()

    @doTryCatch
    def _save_albums(self, username):
        """
        Transverse files on each folder in config.album_names
        :param root: List of roots folders to transverse
        :return : none
        """
        ###
        # Create a new Album for this run. DB name from config.py
        #
        self.album_record = {}.fromkeys(schemas.albums_schema, '')
        self.album_record['name'] = cfg.album_name
        self.album_record['username'] = username
        key = {'name': self.album_record['name']}
        payload = {'username': self.album_record['username']}
        self.album_record['_id'] = self.db.insert_if_not_found("albums", key, payload)
        self.file_record["album_id"] = self.album_record["_id"]

    def _replace_file(self):
        #
        # update the current record
        #
        # build the file query
        query = {'_id': self.data["file_id"]}
        # read record
        if record := self.db.find_one('files', query):
            if self.data.get('file_info', 0):
                record["file_info"] = self.data['file_info']
            if self.data.get('image', 0):
                record["image"] = self.data['image']
            if self.data.get('metadata_id', 0):
                record["metadata_id"] = self.data['metadata_id']
                self.file_record["metdata_id"] = self.data['metadata_id']
            if self.data.get('thumbnail_id', 0):
                record["thumbnail_id"] = self.data['thumbnail_id']
                self.file_record["thumbnail_id"] = self.data['thumbnail_id']
            self.db.replace_one('files', query, record)
        else:
            assert ("Failed")

    def _update_files(self, file_id, payload):
        #
        # update file with payload
        #
        query = {'_id': file_id}
        # self.replace_one('fs.files', query, self.file_record)
        if self.db.update_one('files', query, {'$set': payload}):
            return file_id
        else:
            return None

    def _save_file_to_db(self):
        #
        #generic save file to dv api
        #
        file_id = None
        filename = self.file_record['filename']

        # create file hash for file
        self.file_record['hash_id'] = self._createFileHash(filename)

        # save file if doesn't exist (in case of 2nd run  TODO add a flag to overwrite
        if field_id := self.db.fetch_id('files', {'file_path': self.file_record['file_path']}):
            self.file_record['_id'] = field_id
            payload = {**self.file_record, **self.data}  # dict(zip(self.data.keys(),self.file_record.values()))
            if field_id := self._update_files(self.file_record['_id'], payload):
                self.file_record['_id'] = field_id
                return field_id

        if not field_id:
            self.files_added += 1
            # remove emply id
            del self.file_record['_id']
            if field_id := self.db.saveFiles("files", self.file_record):
                self.file_record['_id'] = field_id
                assert field_id
        return field_id

    def _getFileInfo(self):
        #
        # retrieve the file information
        #
        filenane = self.file_record['filename']
        path = self.file_record['file_path']

        stats_times = {'9': 'changeAt', '8': 'modified', '7': 'accessedAt'}
        stats_st = {'2': 'device', '5': 'owner', '0': 'right', '6': 'size',
                    'ST_UID': 'identifier'}
        Times = namedtuple('Times', ['ino', 'dev', 'device', 'nlink', 'uid', 'owner', 'size', 'accessedAt', 'modified',
                                     'changeAt'])
        fields = ['ino', 'dev', 'device', 'nlink', 'uid', 'owner', 'size', 'accessedAt', 'modified', 'changeAt']
        record = {}
        convert_to_date = lambda x, y: y if x < 6 else time.asctime(time.localtime(stat[x]))
        try:
            stat = os.stat(path)
            record = dict(
                map(lambda x, y: [x, convert_to_date(fields.index(x), y)], fields, [stat[i] for i in range(10)]))
        except IOError as e:
            print(e)
            print(f"Error getting stat info from file {path}")
        return record

    @doTryCatch
    def _savePhotoThumbnail(self):
        """
        For image files like photos, get the metadata of the file
        :param self:
        :param file_path:
        :return:
        """
        file_path = self.file_record['file_path']
        fs = gridfs.GridFS(self.db.db, "image")
        if not os.path.isfile(file_path):
            return

        img = Image.open(file_path)
        img = Watermark().set_watermark(img)
        _bytes_io = create_thumbnail_bytesIO(img)

        if result:= fs.find_one({'filename':os.path.basename(file_path)}):
            fs.delete(file_id=result._id)
        record_id = fs.put(filename=os.path.basename(file_path), contentType='png', data=_bytes_io)
        if record_id:
            self.data['image'] = record_id
            self.file_record['image'] = record_id
        return record_id

        # record_id = self.db.saveImage(file_path, buf)
        # if record_id:
        #     self.data['image'] = record_id
        #     self.file_record['image'] = record_id
        # return record_id

    def prepare_to_save_metadata(self, filename):
        metadata_id = None
        # if os.path.splitext(filename)[1] == '.jpg':
        #     print(filename)
        if self.tag_supported_images(os.path.splitext(filename)[1]):
            ###
            # save TAGS to metadata_tag
            # ###
            if filename == "minha blusa cinza preto e amarelo 2.jpg":
                print("here")
            if metadata_id := self.insertTag(filename):
                assert f"metadata not created for file {filename}"  # supported TAGs image file
        return metadata_id

    def _update_duplicate_tag(self):
        ###
        # find all files with same hash and update the number of duplicates
        ###

        query = {'hash_id': self.file_record['hash_id']}
        count = self.db.count_documents('files', query)

        self.file_record['count'] = count
        if count and count > 1:
            res = self.db.find_all('files', query)
            for record in res:
                payload = {'count': count}
                query = {'_id': record['_id']}
                self.db.update_one('files', query, {"$set": payload})

    def _createFileHash(self, file_hash):
        # create unique hash for each file. if file was copied or renamed the hash remains the same
        file_hash = self.hashfile()
        hash_id = self.db.get_file_hash(file_hash)
        if not hash_id:  # file not in the database yet, insert hash id to hash table
            hash_id = self.db.insert_one_api('hashes', {'hash': file_hash})
        return hash_id

    # get photo_tags for image files
    @doTryCatch
    def _getPhotoTags(self):
        """
        For image files like photos, get the metadata of the file
        :param self:
        :param file_path:
        :return:
        """
        file_path = self.file_record['file_path']
        if not os.path.isfile(file_path):
            return
        tags = {}
        image_types = cfg.supported_images.keys()

        Image.MAX_IMAGE_PIXELS = 933120000
        img = imghdr.what(file_path)
        if img and img in image_types:
            imghdr.test
            image = Image.open(file_path)

            image_item = image._getexif()
            if image_item:
                tags.update(
                    {str(TAGS.get(tag)): str(value) for tag, value in image_item.items()})
                # {str(TAGS.get(tag)): str(value) if isinstance(value, tuple) else value for tag, value in image_item.items()
                #  if TAGS.get(tag) != None})
        Image.MAX_IMAGE_PIXELS = None
        return tags

    @doTryCatch
    def joinDicts(self, dict1, dict2):
        """
        # Joins two dictionaries)
        :param dict1:
        :param dict2:
        :return:
        """
        for key in dict2.keys():
            if key in dict1:
                dict1[key] = dict1[key] + dict2[key]
            else:
                dict1[key] = dict2[key]

    @doTryCatch
    def hashfile(self, blocksize=65536):
        """
        Create a unique hasfile id for the file
        :param path:
        :param blocksize:
        :return: hash number
        """
        afile = open(self.file_record['file_path'], 'rb')
        hasher = hashlib.md5()
        buf = afile.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(blocksize)
        afile.close()
        return hasher.hexdigest()

    @doTryCatch
    def createCatalogs(self):
        """
        Master function to iterate on every root folder
        :return:
        """

        dups = {}
        for root in cfg.included_roots:
            # Iterate the folders given
            if os.path.exists(root):
                self.transverseRoot(root)
            #     # Find the duplicated files and append them to the dups
            #     joinDicts(dups, findDup(i))
            else:
                print('%s is not a valid path, please verify' % root)
                sys.exit()

        return self.files_added

    @doTryCatch
    def insertTag(self, filename):
        tags = self._getPhotoTags()
        if tags and tags.get('Model'):  # check the file if is an image file
            payload = {'image': self.file_record['image']}
            tags.update(payload)
            if tags.get('GPSInfo'):
                tags['GPSInfo'] = str(tags['GPSInfo'])
            tags['filename'] = filename
            metadata_id = self.db.insert_one_api("metadata_tag", tags, bypass_document_validation=True)  # _photoTags
        else:
            metadata_id = None
        return metadata_id
