import sys
import io

#Import required Image library
from PIL import Image, ImageDraw, ImageFont

from .image_wathermark import Watermark

watermark = Watermark()


def create_thumbnail_bytesIO(img):
    """
    Create a trumbnail from image
    :param file_path:
    :return: thumbnail object
    """
    try:
        im_resize = img.resize((200, 200))
        buf = io.BytesIO()
        im_resize.save(buf, format='png')

        return buf

    except IOError as e:
        print("Unable to create image")
        print(e.message)
        sys.exit(1)

