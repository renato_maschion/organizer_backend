import os
from decouple import Config

basedir = os.path.abspath(os.path.dirname(__file__))

# Set up the App SECRET_KEY
SECRET_KEY = Config('SECRET_KEY', default='anykey')

class ProductionConfig(Config):
    DEBUG = False

    # Security
    SESSION_COOKIE_HTTPONLY  = True
    REMEMBER_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_DURATION = 3600

class DebugConfig(Config):
    DEBUG = True

# Load all possible configurations
config_dict = {
    'Production': ProductionConfig,
    'Debug'     : DebugConfig
}
