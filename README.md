# FILE ORGANIZER
this is the first draft of this document. Much more will be added during development.

## Motivation
### Identify duplicated files hanging on hard drives consuming space.

It is common action of making a file copy, rename, move to a temp folder or sandbox location and forget about it.
Regular text files are not a space concern but when we think about raw photo files, which are very common now a dasy, terabytes of space are consumed quickly and it is a nightmare to manage them.

Initialy this tool was designed to deal with photos and images files but it is being extended to other kind of files too.

### Design Approach
Basically, a PYTHON file search will look for files matching the extensions provided, considering the other location parameters bellow:
PARAMS:
* ALBUM name or project name like 'renato's mac-mini pictures'.
* ROOT: list of roots, like ('/Users/renato'. )
* PATH: list of folders like ('Pictures/2019', 'Pictures/2020', )
* EXTENSIONS: list of file extensions like ('jpeg','jpg', )

During the search PYTHON will identify the duplicated files.

* The files data will be added to a DB.
  Initially sqlite db and later updated to Mongodb,

Vue UI for desktop and portables will display the data and allow certains operations like, open the file, delete, rename, etc.

## Tests
A complete set of tests will be added, verifying backend and front end




